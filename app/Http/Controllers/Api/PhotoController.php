<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Photo;
use App\Models\PhotoAssign;
use Auth;

class PhotoController extends Controller
{
    public function index(Request $request)
    {
        $data = [];
        if($request->status == "public"){
            $photo = Photo::where('status', $request->status);
            $photo->when($request->name, function($query) use($request){
                return $query->where('name', 'like', $request->name);
            });
            $photo->when($request->date_time, function($query) use($request){
                return $query->where('created_at', '=', date("Y-m-d H:i:s", strtotime($request->date_time)));
            });
            $photo->when($request->extension, function($query) use($request){
                return $query->where('photo', '!=', null);
            });
            $data = $photo->get();
            if($request->extension){
                $data = [];
                foreach ($photo->get() as $key => $value) {
                    if(explode(".", $value->photo)[1] == $request->extension)
                    {
                        $data[] = $value;
                    }
                }
            }
        } elseif($request->status == "hidden"){
            $photo = Photo::where('user_id', Auth::user()->id);
            $photo->when($request->name, function($query) use($request){
                return $query->where('name', 'like', $request->name);
            });
            $photo->when($request->date_time, function($query) use($request){
                return $query->where('created_at', '=', date("Y-m-d H:i:s", strtotime($request->date_time)));
            });
            $photo->when($request->extension, function($query) use($request){
                return $query->where('photo', '!=', null);
            });
            $data = $photo->get();
            if($request->extension){
                $data = [];
                foreach ($photo->get() as $key => $value) {
                    if(explode(".", $value->photo)[1] == $request->extension)
                    {
                        $data[] = $value;
                    }
                }
            }
        } elseif($request->status == "private"){
            $photo = PhotoAssign::with('photos');
            foreach($photo->get() as $key => $value)
            {
                foreach ($value->photos as $photo) {
                    $data[] = $photo;
                }
            }
        }
        $response = [
            "status" => "success",
            "msg" => "photos",
            "data" => $data
        ];
        return response()->json($response, 200);
    }
    public function store(Request $request)
    {
        if($request->hasFile('photo')){
            $photo =$request->file('photo');
            $filename = time().'.'.$photo->getClientOriginalExtension();
            $request->photo->move(public_path('user/photos'), $filename);
            $filename = time().'.'.$photo->getClientOriginalExtension();
            $photo_data = [
                "name" => $request->name,
                "photo" => $filename,
                "status" => $request->status,
                "user_id" => Auth::user()->id
            ];
            $photo = Photo::create($photo_data);
            if($request->status == "private"){
                $photo_assign_data = [
                    "photo_id" => $photo->id,
                    "user_id" => $request->assign_id
                ];
                PhotoAssign::create($photo_assign_data);
            } 
        }
        $response = [
            "status" => "success",
            "msg" => "Created",
        ];
        return response()->json($response, 201);
    }
}
