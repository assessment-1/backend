<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;

class SearchController extends Controller
{
    public function index(Request $request)
    {
        $data = [];
        if(empty($request) ){
            $data = User::all();
        } else {
            $users = User::query();
            $users->when($request->name, function($query) use($request){
                return $query->where('name', 'like', $request->name);
            });
            $users->when($request->date_time, function($query) use($request){
                return $query->where('created_at', '=', date("Y-m-d H:i:s", strtotime($request->date_time)));
            });
            $users->when($request->status, function($query) use($request){
                return $query->where('status', $request->status);
            });
            $users->when($request->extension, function($query) use($request){
                return $query->where('profile_picture', '!=', null);
            });
            $data = $users->get();
            if($request->extension){
                $data = [];
                foreach ($users->get() as $key => $value) {
                    if(explode(".", $value->profile_picture)[1] == $request->extension)
                    {
                        $data[] = $value;
                    }
                }
            }
            $response = [
                "status" => "success",
                "msg" => "Users Data",
                "data" => $data
            ];
            return response()->json($response, 200);
        }
    }
}
