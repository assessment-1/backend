<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Validator;
use Auth;
use DB; 
use Carbon\Carbon; 
use Illuminate\Support\Str;
use Hash;

class UserController extends Controller
{
    /**
     * Register User
     */
    public function register(Request $request)
    {
       
       
        $validator = Validator::make($request->all(),[ 
            'email' => 'required|email|unique:users',
            'password' => 'required',
            'name' => 'required',
            'age' => 'required',
        ]);
        if($validator->fails())
        {
            $message = $validator->errors();
            return response()->json(['error' => $message], 400);

        }else{
            $user=new User();
            $user->name =$request->name;
            $user->email =$request->email;
            $user->age =$request->age;
            $user->password = bcrypt($request->password);
          
             if ($request->hasfile('profile_picture')) {
                 $profile=$request->file('profile_picture');
                 $filename = time().'.'.$profile->getClientOriginalExtension();
                 $request->profile_picture->move(public_path('user/profile_picture'), $filename);   
                 $user->profile_picture=   $filename;
     
             };
             $saved=$user->save();
             if($saved)
             {
             $token = $user->createToken(config('app.name'))->plainTextToken;
                 return response()->json(['token' => $token], 200);
             }else{
              
                     return response()->json(['error' => 'User not Created'], 401);
                 
             }
        }
        
         
     
      
  
       
  
       
    }


    /**
     * Login User
     */
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(),[ 
            'email' => 'required|email',
            'password' => 'required',
        ]);
        if($validator->fails())
        {
            $message = $validator->errors();
            return response()->json(['error' => $message], 400);

        }else{
            $data = [
                'email' => $request->email,
                'password' => $request->password
            ];
  
        if (auth()->attempt($data)) {
            $token = auth()->user()->createToken(config('app.name'))->plainTextToken;
            return response()->json(['token' => $token], 200);
        } else {
            return response()->json(['error' => 'Unauthorised'], 401);
        }
    }
    }
    public function update(Request $request)
    {
       
        $validator = Validator::make($request->all(),[ 
            'email' => 'required|email|unique:users'.auth('sanctum')->user()->id,
            'password' => 'required',
            'name' => 'required',
            'age' => 'required',
        ]);
        if($validator->fails())
        {
            $message = $validator->errors();
            return response()->json(['error' => $message], 400);

        }else{
            $user=User::find(Auth::user()->id);
            $user->name =$request->name;
            $user->email =$request->email;
            $user->age =$request->age;
            $user->password = bcrypt($request->password);
          
             if ($request->hasfile('profile_picture')) {
                 $profile=$request->file('profile_picture');
                 $filename = time().'.'.$profile->getClientOriginalExtension();
                 $request->profile_picture->move(public_path('user/profile_picture'), $filename);   
                 $user->profile_picture=   $filename;
     
             };
             $saved=$user->save();
             if($saved)
             {
             $token = $user->createToken(config('app.name'))->plainTextToken;
                 return response()->json(['token' => $token], 200);
             }else{
              
                     return response()->json(['error' => 'User not Created'], 401);
                 
             }
        }
    }
    public function logout(Request $request)
    {
        $request->user()->currentAccessToken()->delete();
        return response()->json(['success' => "User Logout Successfully!"], 200);
    }

    public function forgotPassword(Request $request)
    {

        $validator = Validator::make($request->all(),[ 
            'email' => 'required|email|exists:users',

        ]);
        if($validator->fails())
        {
            $message = $validator->errors();
            return response()->json(['error' => $message], 400);

        }else{
          $token = Str::random(64);
         $userToken= DB::table('password_resets')->insert([
              'email' => $request->email, 
              'token' => $token, 
              'created_at' => Carbon::now()
            ]);
         
            return response()->json(['token' => $token], 200);
        }
    }

    public function resetPassword(Request $request)
    {
        
       
        $validator = Validator::make($request->all(),[ 
            'email' => 'required|email',
            'password' => 'required',
        ]);
        if($validator->fails())
        {
            $message = $validator->errors();
            return response()->json(['error' => $message], 400);

        }else{
      

        $updatePassword = DB::table('password_resets')
                            ->where([
                              'email' => $request->email, 
                              'token' => $request->token
                            ])
                            ->first();

        if(!$updatePassword){
            return response()->json(['success' => "Invalid Token!"], 200);
        }

        $user = User::where('email', $request->email) ->update(['password' => Hash::make($request->password)]);

        DB::table('password_resets')->where(['email'=> $request->email])->delete();
        return response()->json(['success' => "Your Password Change Sussfully!"], 200);
    }

    }
    public function userData()
    {
        $user = User::where('id', Auth::user()->id)->first();
        return response()->json($user, 200);
    }
}
