<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Photo;

class PhotoAssign extends Model
{
    use HasFactory;
    protected $fillable = [
        "photo_id",
        "user_id"
    ];

    public function photos()
    {
        return $this->hasMany(Photo::class, 'id', 'photo_id');
    }
}
