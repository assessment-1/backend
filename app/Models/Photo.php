<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    use HasFactory;
    protected $fillable = [
        "photo",
        "status",
        "user_id",
        "name",
    ];

    public function getPhotoAttribute($value)
    {
        return url("user/photos/".$value);
    }

}
