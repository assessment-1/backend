<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
Use App\Http\Controllers\Api\UserController;
Use App\Http\Controllers\Api\SearchController;
Use App\Http\Controllers\Api\PhotoController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


 
Route::post('register', [UserController::class, 'register']);
Route::post('login', [UserController::class, 'login']);
Route::post('forgot-password', [UserController::class, 'forgotPassword']);
Route::post('reset-password', [UserController::class, 'resetPassword']);
Route::post('/users/list', [SearchController::class, 'index']);

Route::middleware('auth:sanctum')->group(function () {
    
    Route::get('user', [UserController::class, 'userData']);
    Route::post('update', [UserController::class, 'update']);
    Route::post('logout', [UserController::class, 'login']);

    Route::post('/photo/list', [PhotoController::class, 'index']);
    Route::post('/photo/create', [PhotoController::class, 'store']);
    
});